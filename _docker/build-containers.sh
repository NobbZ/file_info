#!/usr/bin/env zsh

set -ex

# docker_folders=$(find _docker -mindepth 1 -maxdepth 1 -type d -print0)

find _docker -mindepth 1 -maxdepth 1 -type d -print0 | sort -z | while read -r -d $'\0' d; do
    tag="${d#_docker/}"
    docker build -t registry.gitlab.com/nobbz/file_info:${tag} ${d}

    docker push registry.gitlab.com/nobbz/file_info:${tag}
done
