defmodule FileInfo.Compat.String do
  @ex_version System.version()

  defmacro strip(arg) do
    case Version.compare(@ex_version, "1.3.0") do
      :lt -> quote(do: String.strip(unquote(arg)))
      _ -> quote(do: String.trim(unquote(arg)))
    end
  end
end
